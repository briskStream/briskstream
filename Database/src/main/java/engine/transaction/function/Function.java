package engine.transaction.function;

/**
 * Push down function
 */
public abstract class Function {
    public long delta;
    public double[] new_value;

}
