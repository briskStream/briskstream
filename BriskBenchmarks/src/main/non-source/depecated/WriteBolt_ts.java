package applications.bolts.mb.deprecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.Checkpointable;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Marker;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerTStream;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;

import static applications.Constants.DEFAULT_STREAM_ID;
import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;

public class WriteBolt_ts extends TransactionProcessBolt implements Checkpointable {
    private static final Logger LOG = LoggerFactory.getLogger(WriteBolt_ts.class);
    private static final long serialVersionUID = -5968750340131744744L;
    final double useful_time = 1477.0;//pre-measured.
    LinkedList<Long> gap = new LinkedList<>();
    private List<MicroEvent> holder = new LinkedList<>();

    public WriteBolt_ts(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, ExecutionGraph graph) {
        super.initialize(thread_Id, graph);
        transactionManager = new TxnManagerTStream(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());
    }

    /**
     * Multi-keys transaction.
     *
     * @param enqueue_time
     * @param index_time
     * @param keys
     * @param bid
     * @throws DatabaseException
     */
    private void txn_request(double[] enqueue_time, double[] index_time, int[] keys, List<DataBox>[] values, long bid) throws DatabaseException {

        txn_context = new TxnContext(this.context.getThisComponentId(), this.fid, bid, index_time);//create a new txn_context for this new transaction.
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            //it simply construct the operations and return.
            transactionManager.Asy_WriteRecord(txn_context, "MicroTable", String.valueOf(keys[i]), values[i], enqueue_time);//asynchronously return.
        }
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException, BrokenBarrierException {
        long bid = in.getBID();
        String componentId = context.getThisComponentId();
        if (in.isMarker()) {
//            //LOG.DEBUG("Starts TP evaluation..");

            long start2 = System.nanoTime();
            transactionManager.start_evaluate(this.context.getThisTaskId(), this.fid, bid);//start lazy evaluation in transaction manager.
            long start3 = System.nanoTime();

            forward_checkpoint(in.getSourceTask(), DEFAULT_STREAM_ID, bid, in.getMarker());


            if (metrics.measure) {

                double queue_time = 0;

                double index_time = 0;
                for (MicroEvent event : holder) {
                    queue_time += event.enqueue_time[0];
                    index_time += event.index_time[0];
                }

                double total_time = (useful_time * holder.size() + (start3 - start2) + index_time + queue_time) / 1E6;

                metrics.useful_time.get(componentId).addValue(useful_time * holder.size() / total_time);//store percentage directly.

                metrics.index_time.get(componentId).addValue(index_time / total_time);//store percentage directly.

                metrics.wait.get(componentId).addValue((start3 - start2) / total_time);//store percentage directly.


            }


            holder.clear();//all tuples in the holder is finished.


        } else {
            MicroEvent event = generateEvent(bid);
            txn_request(event.enqueue_time, event.index_time, event.getKeys(), event.getValues(), event.getTimestamp());
            holder.add(event);//just for record.
            collector.force_emit(event.getTimestamp(), event);//the tuple is immediately finished.
        }
    }

    @Override
    public void forward_checkpoint(int sourceId, long bid, Marker marker) throws InterruptedException {
        this.collector.broadcast_marker(bid, marker);//bolt needs to broadcast_marker
    }

    @Override
    public void ack_checkpoint(Marker marker) {
        this.collector.broadcast_ack(marker);//bolt needs to broadcast_ack
    }
}
