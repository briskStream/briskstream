package applications.bolts.mb.deprecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.SchemaRecordRef;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerTStream;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;

import static applications.Constants.DEFAULT_STREAM_ID;
import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;

public class ReadBolt_ts extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(ReadBolt_ts.class);
    private static final long serialVersionUID = -5968750340131744744L;
//    LinkedList<Long> gap = new LinkedList<>();
    double queue_time = 0;//time spend in enqueueing all events in current epoch.
    double index_time = 0;
    private List<MicroEvent> holder = new LinkedList<>();

    public ReadBolt_ts(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, ExecutionGraph graph) {
        super.initialize(thread_Id, graph);
        transactionManager = new TxnManagerTStream(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());

    }

    /**
     * Multiple keys.
     *
     * @param event
     * @param fid
     * @param bid
     * @throws DatabaseException
     */
    private void txn_request(MicroEvent event, int fid, long bid) throws DatabaseException {
        txn_context = new TxnContext(this.context.getThisComponentId(), this.fid, bid, event.index_time);//create a new txn_context for this new transaction.

        for (int i = 0; i < NUM_ACCESSES; ++i) {
            //it simply construct the operations and return.
            transactionManager.Asy_ReadRecord(txn_context, "MicroTable", String.valueOf(event.getKeys()[i]), event.getRecord_refs()[i], event.enqueue_time);
        }
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException, BrokenBarrierException {
        long bid = in.getBID();
        String componentId = context.getThisComponentId();
        if (in.isMarker()) {
            ////LOG.DEBUG("Starts TP evaluation.. Currently holding: " + holder.size() + " transactions.");
            long start2 = System.nanoTime();
            transactionManager.start_evaluate(this.context.getThisTaskId(), this.fid, bid);//start lazy evaluation in transaction manager.

            long start3 = System.nanoTime();
            for (MicroEvent event : holder) {
                int sum = 0;
                for (int i = 0; i < NUM_ACCESSES; ++i) {
                    SchemaRecordRef ref = event.getRecord_refs()[i];
                    DataBox dataBox = ref.record.getValues().get(1);
                    int read_result = Integer.parseInt(dataBox.getString().trim());
                    sum += read_result;
                }
                queue_time += event.enqueue_time[0];
                index_time += event.index_time[0];
                collector.force_emit(event.getTimestamp(), sum);//the tuple is finished.
            }
            long useful_time = System.nanoTime() - start3;///time spend in performing computing tasks of all events in current epoch.

            forward_checkpoint(in.getSourceTask(), DEFAULT_STREAM_ID, bid, in.getMarker());

            if (metrics.measure) {

                double total_time = (useful_time + (start3 - start2) + index_time + queue_time) / 1E6;

                metrics.useful_time.get(componentId).addValue(useful_time / total_time);//store percentage directly.

                metrics.index_time.get(componentId).addValue(index_time / total_time);//store percentage directly.

                metrics.wait.get(componentId).addValue((start3 - start2) / total_time);//store percentage directly.

                queue_time = 0;

                index_time = 0;
            }

            holder.clear();//all tuples in the holder is finished.

        } else {
            MicroEvent event = generateEvent(bid);
            txn_request(event, this.fid, bid);
            holder.add(event);//mark the tuple as ``in-complete"
        }
    }
}
