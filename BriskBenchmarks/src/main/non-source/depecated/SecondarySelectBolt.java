package applications.bolts.mb.depecated;

import applications.param.MicroEvent;
import applications.util.datatypes.StreamValues;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import engine.Database;
import engine.DatabaseException;
import engine.storage.SchemaRecords;
import engine.transaction.dedicated.TxnManagerTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static engine.Meta.MetaTypes.AccessType.READ_ONLY;

public class SecondarySelectBolt extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(SecondarySelectBolt.class);
    private static final long serialVersionUID = -4440029624518687925L;
    LinkedList<Long> gap = new LinkedList<>();

    public SecondarySelectBolt(int fid) {
        super(LOG, fid);
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        super.initialize(thisTaskId, graph);
        //here, we select different transaction manager.
        //let's start from Lock based.

        transactionManager =
                //no ordering guarantee.
//				new TxnManagerLock(db.getStorageManager());
//				new TxnManagerOcc(db.getStorageManager());
                new TxnManagerTo(db.getStorageManager(), this.context.getThisComponentId(), thisTaskId, this.context.getThisComponent().getNumTasks());


        //ordering guarantee.
//				new TxnManagerOrderLock(db.getStorageManager(), lock);
//				new TxnManagerOccOcc(db.getStorageManager(), orderValidate);
//		final int id = this.context.getThisTaskId();
//		NUM_ACCESSES;
//		this.context.getNUMTasks();

    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        final long bid = in.getBID();
        MicroEvent Event = (MicroEvent) in.getValue(0);
        for (int i = NUM_ACCESSES / 2; i < NUM_ACCESSES; ++i) {
            secondary_select(db, String.valueOf(Event.getKeys()[i]));
        }


        collector.emit(bid, new StreamValues(in));
    }

    private SchemaRecords secondary_select(Database db, String secondary_key) {
        SchemaRecords records = new SchemaRecords(100);
        final boolean rt = transactionManager.SelectRecords(db, txn_context, "MicroTable", 0, secondary_key, records, READ_ONLY, gap);
        assert (rt);
        return null;//should not go here.
    }


}
