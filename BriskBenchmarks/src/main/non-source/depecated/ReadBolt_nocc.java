package applications.bolts.mb.deprecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.Checkpointable;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.TransferTuple;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.SchemaRecordRef;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.TxnManagerLock;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;
import static engine.Meta.MetaTypes.AccessType.READ_ONLY;

public class ReadBolt_nocc extends TransactionProcessBolt implements Checkpointable {
    private static final Logger LOG = LoggerFactory.getLogger(ReadBolt_nocc.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    long retry_start = 0;
    long retry_time = 0;

    public ReadBolt_nocc(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, ExecutionGraph graph) {
        super.initialize(thread_Id, graph);
        transactionManager = new TxnManagerLock(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());

    }

    private boolean txn_request(MicroEvent event, long bid) throws DatabaseException {

        for (int i = 0; i < NUM_ACCESSES; ++i) {
            boolean rt = transactionManager.SelectKeyRecord(txn_context, "MicroTable", String.valueOf(event.getKeys()[i]), event.getRecord_refs()[i], READ_ONLY);
            if (rt) {
                assert event.getRecord_refs()[i].record != null;
            } else {
                if (txn_context.is_retry_) {//re_try again.
                    retry_time += System.nanoTime() - retry_start;
                }
                txn_context.is_retry_ = true;
                retry_start = System.nanoTime();
                return false;
            }
        }
        return true;
    }


    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {

        long bid = in.getBID();
        MicroEvent event = generateEvent(bid);
        //begin transaction processing.
        long start = System.nanoTime();
        String componentId = context.getThisComponentId();

        txn_context = new TxnContext(this.context.getThisComponentId(), this.fid, bid);
        boolean rt;
        do {
            rt = txn_request(event, bid);
        } while (!rt);


        transactionManager.CommitTransaction(txn_context);//always success..

        long start2 = System.nanoTime();
        int sum = 0;
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            SchemaRecordRef ref = event.getRecord_refs()[i];
            DataBox dataBox = ref.record.getValues().get(1);
            int read_result = Integer.parseInt(dataBox.getString().trim());
            sum += read_result;
        }

        if (metrics.measure) {
            long end = System.nanoTime();
            double total_time = (end - start) / 1E6;

            metrics.useful_time.get(componentId).addValue((end - start2) / total_time);//store percentage directly.

            if (txn_context.is_retry_) {
                if (retry_time != 0) {//multiple re-try
                    metrics.abort_time.get(context.getThisComponentId()).addValue(retry_time / total_time);
                    retry_time = 0;
                    retry_start = 0;
                } else {
                    metrics.abort_time.get(context.getThisComponentId()).addValue((start2 - retry_start) / total_time);
                    retry_start = 0;
                }
            }

            metrics.ts_allocation.get(componentId).addValue(txn_context.ts_allocation / total_time);//store percentage directly.

            metrics.index_time.get(componentId).addValue(txn_context.index_time / total_time);//store percentage directly.

        }

        collector.force_emit(event.getTimestamp(), sum);//the tuple is finished.
    }

    public void execute(TransferTuple in) throws InterruptedException {
        //not ready yet.
    }
}
