package applications.bolts.mb.depecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Fields;
import brisk.execution.runtime.tuple.impl.Marker;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.SplittableRandom;

import static engine.content.Content.CCOption_TStream;

public class MicroEventSequencer extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(MicroEventSequencer.class);
    private static final long serialVersionUID = -5968750340131744744L;
    //the following are used for checkpoint
    protected int myiteration = 0;//start from 1st iteration.
    protected boolean success = true;
    protected long boardcast_time;
    //    Random r = new Random();
    transient SplittableRandom rnd;
    private int ccOption;
    private long bid = 0;
    private Random r = new Random();
    //different R-W ratio.
    //just enable one of the decision array
    private boolean[]
            read_decision = {true, true, true, true, true, true, true, true};// all read.

    public MicroEventSequencer(int fid) {
        super(LOG, fid);
        state = new ValueState();
        this.setFields("ReadStream", new Fields("MicroEvent"));
        this.setFields("WriteStream", new Fields("MicroEvent"));
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {

        rnd = new SplittableRandom(1234);
        ccOption = config.getInt("CCOption", 0);
        bid = 0;
    }
    //	read_decision = {false, false, false, false, true, true, true, true};//equal r-w ratio.

//            read_decision = {false, false, true, true, true, true, true, true};//25% W, 75% R.
    //  read_decision = {false, false, false, false, false, false, true, true};//75% W, 25% R.

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {

        MicroEvent[] Events = (MicroEvent[]) in.getValue(0);

        for (MicroEvent Event : Events) {

            Event.setTimestamp(bid);

            int i = r.nextInt(8);
            boolean flag = read_decision[i];

            if (flag) {
                collector.emit_single("ReadStream", bid, Event);
            } else {
                collector.emit_single("WriteStream", bid, Event);
            }

            if (ccOption == CCOption_TStream)
                forward_checkpoint(-1, bid, null); // This is required by T-Stream.

            bid++;

            if (bid == Long.MAX_VALUE) {
                this.getContext().getGraph().getSink().op.forceStop();
                this.getContext().join();
            }
        }
    }

    @Override
    public void forward_checkpoint(int sourceId, long bid, Marker marker) throws InterruptedException {
        if (clock.tick(myiteration) && success) {//emit marker tuple
//			forwardResultAndMark(streamId, values, bid_counter++ % bid_end);
//			final long msgId = bid_counter++;//++ % bid_end;
//			LOG.info(executor.getOP_full() + " emit marker of: " + myiteration + " @" + DateTime.now());
//			marker = new Marker(streamId, boardcast_time, msgId, myiteration);
            collector.create_marker_boardcast(boardcast_time, bid, myiteration);
            myiteration++;
            success = false;
            boardcast_time = System.nanoTime();
        }
    }

    @Override
    public void forward_checkpoint(int sourceTask, String streamId, long bid, Marker marker) throws InterruptedException {
        if (clock.tick(myiteration) && success) {//emit marker tuple
//			forwardResultAndMark(streamId, values, bid_counter++ % bid_end);
//			final long msgId = bid_counter++;//++ % bid_end;
//			LOG.info(executor.getOP_full() + " emit marker of: " + myiteration + " @" + DateTime.now());
//			marker = new Marker(streamId, boardcast_time, msgId, myiteration);
            collector.create_marker_boardcast(boardcast_time, streamId, bid, myiteration);
            myiteration++;
            success = false;
            boardcast_time = System.nanoTime();
        }
    }

    @Override
    public void ack_checkpoint(Marker marker) {
        //Do something to clear past state. (optional)
        success = true;//I can emit next marker.
    }


    public void end_measure() {
        final long end = System.nanoTime();
        final double delay_ms = (end - boardcast_time) / 1E6;
        LOG.info(executor.getOP_full() + " takes " + delay_ms + " ms to finish last forward_checkpoint.");
    }

}
