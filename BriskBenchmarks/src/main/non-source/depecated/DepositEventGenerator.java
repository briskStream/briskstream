package applications.bolts.ct.deprecated;


import applications.param.DepositEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.SplittableRandom;

import static applications.constants.CrossTableConstants.Constant.*;

public class DepositEventGenerator extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(DepositEventGenerator.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    //    Random r = new Random();
    transient SplittableRandom rnd;

//    private String rightpad(String text, int length) {
//        return String.format("%-" + length + "." + length + "s", text);
//    }

//    private String GenerateValue(int key) {
//        return rightpad(String.valueOf(key), VALUE_LEN);
//    }

    public DepositEventGenerator(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        rnd = new SplittableRandom(1234);
    }

    protected DepositEvent randomDepositEvent(long bid, SplittableRandom rnd) {
        final int account = rnd.nextInt(NUM_ACCOUNTS);
        final int book = rnd.nextInt(NUM_BOOK_ENTRIES);
        final long accountsDeposit = rnd.nextLong(MAX_ACCOUNT_TRANSFER);
        final long deposit = rnd.nextLong(MAX_BOOK_TRANSFER);

        return new DepositEvent(
                bid, ACCOUNT_ID_PREFIX + account,
                BOOK_ENTRY_ID_PREFIX + book,
                accountsDeposit,
                deposit);
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();

        if (in.isMarker()) {
            forward_checkpoint(in.getSourceTask(), bid, in.getMarker());
        } else {
            DepositEvent event = randomDepositEvent(bid, rnd);
            collector.force_emit(bid, event);
        }
    }
}
