package applications.bolts.mb.depecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.TransferTuple;
import brisk.execution.runtime.tuple.impl.Tuple;
import engine.DatabaseException;
import engine.storage.SchemaRecord;
import engine.storage.datatype.DataBox;
import engine.storage.datatype.IntDataBox;
import engine.storage.datatype.StringDataBox;
import engine.transaction.dedicated.TxnManagerTo;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;

public class InsertBolt extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(InsertBolt.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();

    public InsertBolt(int fid) {
        super(LOG, fid);
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        super.initialize(thisTaskId, graph);
        transactionManager =

                //no ordering guarantee.
//				new TxnManagerLock(db.getStorageManager());
//				new TxnManagerOcc(db.getStorageManager());
                new TxnManagerTo(db.getStorageManager(), this.context.getThisComponentId(), thisTaskId, this.context.getThisComponent().getNumTasks());

        //ordering guarantee.
//				new TxnManagerOrderLock(db.getStorageManager(), lock);
//				new TxnManagerOccOcc(db.getStorageManager(), orderValidate);

    }

    /**
     * "put": "INSERT INTO MicroTable VALUES (?, ?)",
     *
     * @param record
     */
    private void insert(SchemaRecord record) throws DatabaseException {

        //this while loop is for conservative based CC.
        while (!transactionManager.InsertRecord(txn_context, "MicroTable", record, gap) && !Thread.currentThread().isInterrupted()) {
            txn_context.is_retry_ = true;
        }
    }


    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }


    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {

        //begin transaction processing.
        txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, in.getBID());//create a new txn_context for this new transaction.
        MicroEvent Event = (MicroEvent) in.getValue(0);
        do {

            for (int i = NUM_ACCESSES / 2; i < NUM_ACCESSES; ++i) {
                int key = Event.getKeys()[i];
                String value = GenerateValue(key);
                List<DataBox> values = new ArrayList<>();
                values.add(new IntDataBox(key));
                values.add(new StringDataBox(value, value.length()));
                SchemaRecord schemaRecord = new SchemaRecord(values);
                //				//LOG.DEBUG("Insert d_record with key of:" + key);
                insert(schemaRecord);
            }
        } while (!transactionManager.CommitTransaction(txn_context)&& !Thread.currentThread().isInterrupted());
        //end transaction processing.
//		//LOG.DEBUG(this.context.getThisTaskId() + " put finish one transaction of: " + bid + " at:\t" + DateTime.now());

        //operator communication
        collector.emit(in.getBID(), in.getValue(0));
    }


    public void execute(TransferTuple in) throws InterruptedException {
        //undefined.
    }

}
