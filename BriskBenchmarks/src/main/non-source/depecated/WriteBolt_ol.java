package applications.bolts.mb.depecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.Checkpointable;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerOrderLock;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;
import static engine.Meta.MetaTypes.AccessType.READ_WRITE;

public class WriteBolt_ol extends TransactionProcessBolt implements Checkpointable {
    private static final Logger LOG = LoggerFactory.getLogger(WriteBolt_ol.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    long retry_start = 0;

    public WriteBolt_ol(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        super.initialize(thisTaskId, graph);
        transactionManager = new TxnManagerOrderLock(db.getStorageManager(), this.context.getThisComponentId(), thisTaskId, this.context.getThisComponent().getNumTasks());
    }

    private boolean txn_request(MicroEvent Event, long bid) throws DatabaseException {

        for (int i = 0; i < NUM_ACCESSES; ++i) {
            String key = String.valueOf(Event.getKeys()[i]);
            boolean rt = transactionManager.SelectKeyRecord(txn_context, "MicroTable", key, Event.getRecord_refs()[i], READ_WRITE);
            if (rt) {
                assert Event.getRecord_refs()[i].record != null;
            } else {
                txn_context.is_retry_ = true;
                if (retry_start == 0)//it may retry multiple times.
                    retry_start = System.nanoTime();
                return false;
            }
        }
        return true;
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();
        MicroEvent event = generateEvent(bid);
        //begin transaction processing.
        long start = System.nanoTime();

        txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, bid);

        boolean rt;
        do {
            rt = txn_request(event, event.getTimestamp());
        } while (!rt);

        transactionManager.CommitTransaction(txn_context);//always success..

        long start2 = System.nanoTime();
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            List<DataBox> values = event.getValues()[i];
            event.getRecord_refs()[i].record.updateValues(values);
        }

        if (metrics.measure) {
            long end = System.nanoTime();
            metrics.useful_time.get(context.getThisComponentId()).addValue((end - start2));
            if (txn_context.is_retry_)
                metrics.abort_time.get(context.getThisComponentId()).addValue((end - retry_start));
            metrics.total_time.get(context.getThisComponentId()).addValue((end - start));
        }
        collector.force_emit(event.getTimestamp(), event);//the tuple is finished.
    }
}
