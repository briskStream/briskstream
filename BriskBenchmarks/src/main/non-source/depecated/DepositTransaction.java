package applications.bolts.ct;


import applications.param.DepositEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.transaction.dedicated.ordered.TxnManagerTStream;
import engine.transaction.function.INC;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.SplittableRandom;
import java.util.concurrent.BrokenBarrierException;

/**
 * Read and write are bundled together.
 */
public class DepositTransaction extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(DepositTransaction.class);
    private static final long serialVersionUID = -5968750340131744744L;
    transient SplittableRandom rnd;

    public DepositTransaction(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    @Override
    public void initialize(int thread_Id, int thisTaskId, ExecutionGraph graph) {
        transactionManager = new TxnManagerTStream(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());
        rnd = new SplittableRandom(1234);
    }


    /**
     * @param Event
     * @param bid   Event.Item_value.d_record.getValues().get(1).getLong()
     * @throws DatabaseException
     */
    private void txn_request(DepositEvent Event, long bid) throws DatabaseException {
        //begin transaction processing.
//        //LOG.DEBUG("start txn processing for bid: " + bid);
        txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, bid);//create a new txn_context for this new transaction.
        transactionManager.Asy_ModifyRecord(txn_context, "accounts", Event.getAccountId(), Event.getAccountId(), new INC(Event.getAccountTransfer()));// read and modify the account itself.
        transactionManager.Asy_ModifyRecord(txn_context, "bookEntries", Event.getBookEntryId(), Event.getBookEntryId(), new INC(Event.getBookEntryTransfer()));// read and modify the asset itself.
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException, BrokenBarrierException {
        long bid = in.getBID();
        if (in.isMarker()) {
            transactionManager.start_evaluate(this.context.getThisTaskId(), this.fid, bid);//start lazy evaluation in transaction manager.
            forward_checkpoint(in.getSourceTask(), bid, in.getMarker());
        } else {
            DepositEvent Event = randomDepositEvent(bid, rnd);//(DepositEvent) in.getValue(0);
            txn_request(Event, bid);
        }
    }
}
