package applications.bolts.mb.deprecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.SchemaRecordRef;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerOrderLockBlocking;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;
import static engine.Meta.MetaTypes.AccessType.READ_ONLY;

public class ReadBolt_olb extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(ReadBolt_olb.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();

    public ReadBolt_olb(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, ExecutionGraph graph) {
        super.initialize(thread_Id, graph);
        transactionManager = new TxnManagerOrderLockBlocking(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());
    }

    private void txn_request(MicroEvent Event, long bid) throws DatabaseException {
        for (int i = 0; i < NUM_ACCESSES; ++i)
            transactionManager.SelectKeyRecord_noLock(txn_context, "MicroTable", String.valueOf(Event.getKeys()[i]), Event.getRecord_refs()[i], READ_ONLY);
    }

    private void lock_ahead(MicroEvent Event, long bid) throws DatabaseException {
        for (int i = 0; i < NUM_ACCESSES; ++i)
            transactionManager.lock_ahead(txn_context, "MicroTable", String.valueOf(Event.getKeys()[i]), Event.getRecord_refs()[i], READ_ONLY);
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();
        MicroEvent event = generateEvent(bid);

        String componentId = context.getThisComponentId();
        //begin transaction processing.
        long start = System.nanoTime();

        txn_context = new TxnContext(componentId, this.fid, bid);

        //ensures that locks are added in the event sequence order.
        transactionManager.getOrderLock().blocking_wait(bid);
        lock_ahead(event, bid);
        transactionManager.getOrderLock().advance();
        long order_wait = 0;
        if (metrics.measure) {
            long end = System.nanoTime();
            order_wait = (end - start);
        }

        txn_request(event, bid);

        transactionManager.CommitTransaction(txn_context);

        long start2 = System.nanoTime();
        int sum = 0;
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            SchemaRecordRef ref = event.getRecord_refs()[i];
            DataBox dataBox = ref.record.getValues().get(1);
            int read_result = Integer.parseInt(dataBox.getString().trim());
            sum += read_result;
        }

        if (metrics.measure) {
            long end = System.nanoTime();
            double total_time = (end - start) / 1E6;

            metrics.useful_time.get(componentId).addValue((end - start2) / total_time);//store percentage directly.

//            metrics.ts_allocation.get(componentId).addValue(txn_context.ts_allocation / exe_time);//store percentage directly.

            metrics.index_time.get(componentId).addValue(txn_context.index_time / total_time);//store percentage directly.

            metrics.wait.get(componentId).addValue((txn_context.lock_time + order_wait) / total_time);//store percentage directly.
        }

        LOG.trace("BID:" + bid + " is finished.");
        collector.force_emit(event.getTimestamp(), sum);//the tuple is finished.
    }
}
