package applications.bolts.mb.depecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.Checkpointable;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.SchemaRecord_ref;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerOrderLock;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;
import static engine.Meta.MetaTypes.AccessType.READ_ONLY;

public class ReadBolt_ol extends TransactionProcessBolt implements Checkpointable {
    private static final Logger LOG = LoggerFactory.getLogger(ReadBolt_ol.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    long retry_start = 0;

    public ReadBolt_ol(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        super.initialize(thisTaskId, graph);
        transactionManager = new TxnManagerOrderLock(db.getStorageManager(), this.context.getThisComponentId(), thisTaskId, this.context.getThisComponent().getNumTasks());
    }

    private boolean txn_request(MicroEvent event, long bid) throws DatabaseException {

        for (int i = 0; i < NUM_ACCESSES; ++i) {
            boolean rt = transactionManager.SelectKeyRecord(txn_context, "MicroTable", String.valueOf(event.getKeys()[i]), event.getRecord_refs()[i], READ_ONLY);
            if (rt) {
                assert event.getRecord_refs()[i].record != null;
            } else {
                txn_context.is_retry_ = true;
                if (retry_start == 0)//it may retry multiple times.
                    retry_start = System.nanoTime();
                return false;
            }
        }
        return true;
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();
        MicroEvent event = generateEvent(bid);

        //begin transaction processing.
        long start = System.nanoTime();

        txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, bid);

        //ensures that locks are added in the event sequence order.
        transactionManager.getOrderLock().blocking_wait(bid);
        if (metrics.measure) {
            long end = System.nanoTime();
            metrics.order_wait.get(context.getThisComponentId()).addValue((end - start));
        }

        txn_request(event, bid);
        transactionManager.getOrderLock().advance();

        while (!transactionManager.CommitTransaction(txn_context)) {
        }

        long start2 = System.nanoTime();
        int sum = 0;
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            SchemaRecord_ref ref = event.getRecord_refs()[i];
            DataBox dataBox = ref.record.getValues().get(1);
            int read_result = Integer.parseInt(dataBox.getString().trim());
            sum += read_result;
        }

        if (metrics.measure) {
            long end = System.nanoTime();
            metrics.useful_time.get(context.getThisComponentId()).addValue((end - start2));
            if (txn_context.is_retry_)
                metrics.abort_time.get(context.getThisComponentId()).addValue((end - retry_start));

            metrics.total_time.get(context.getThisComponentId()).addValue((end - start));
        }

        //LOG.DEBUG("BID:" + bid + " is finished.");
        collector.force_emit(event.getTimestamp(), sum);//the tuple is finished.
    }
}
