package applications.bolts.mb.depecated;


import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.TransferTuple;
import brisk.execution.runtime.tuple.impl.Fields;
import brisk.execution.runtime.tuple.impl.Tuple;
import engine.DatabaseException;
import engine.storage.SchemaRecord;
import engine.storage.SchemaRecord_ref;
import engine.transaction.dedicated.TxnManagerTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static applications.constants.MicroBenchmarkConstants.Field.TEXT;
import static engine.Meta.MetaTypes.AccessType.READ_ONLY;
import static engine.Meta.MetaTypes.AccessType.READ_WRITE;

public class KeySelectorBolt extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(KeySelectorBolt.class);
    private static final long serialVersionUID = 6406285763553583487L;
    LinkedList<Long> gap = new LinkedList<>();
    private int start, bound;

    public KeySelectorBolt(int fid) {
        super(LOG, fid);
    }

    @Override
    public Fields getDefaultFields() {
        return new Fields(TEXT);
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        super.initialize(thisTaskId, graph);
        //here, we select different transaction manager.
        //let's start from Lock based.

        transactionManager =
                //no ordering guarantee.
//				new TxnManagerLock(db.getStorageManager());
//				new TxnManagerOcc(db.getStorageManager());
                new TxnManagerTo(db.getStorageManager(), this.context.getThisComponentId(), thisTaskId, this.context.getThisComponent().getNumTasks());


        //ordering guarantee.
//				new TxnManagerOrderLock(db.getStorageManager(), lock);
//				new TxnManagerOccOcc(db.getStorageManager(), orderValidate);
//		final int id = this.context.getThisTaskId();
//		NUM_ACCESSES;
//		this.context.getNUMTasks();

    }

    /**
     * AsySelectKeyRecord(&txn_context, MICRO_TABLE_ID, std::string((char*)(&micro_param->keys_[i]), sizeof(int64_t)), d_record, READ_ONLY)
     *
     * @param key
     */
    private SchemaRecord read_only(String key) throws DatabaseException {
        SchemaRecord_ref record_ref = new SchemaRecord_ref();

//		while (!AsySelectKeyRecord(db, txn_context, "MicroTable", key, record_ref, READ_ONLY)) {
//			;
//		}
        while (!transactionManager.SelectKeyRecord(txn_context, "MicroTable", key, record_ref, READ_ONLY)
                && !Thread.currentThread().isInterrupted()) {
            txn_context.is_retry_ = true;
        }

        return record_ref.record;
    }


    /**
     * AsySelectKeyRecord(&context_, MICRO_TABLE_ID, std::string((char*)(&micro_param->keys_[i]), sizeof(int64_t)), d_record, READ_WRITE)
     *
     * @param key
     */
    private SchemaRecord read_write(String key) throws DatabaseException {
        SchemaRecord_ref record_ref = new SchemaRecord_ref();


        while (!transactionManager.SelectKeyRecord(txn_context, "MicroTable", key, record_ref, READ_WRITE)
                && !Thread.currentThread().isInterrupted()) {
            txn_context.is_retry_ = true;
        }

        return record_ref.record;
    }


    @Override
    public void execute(Tuple in) throws InterruptedException {
//		int bound = in.length;
//		final long bid = in.getBID();
//
//		//begin transaction processing.
//		txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, bid);
//		do {
//			for (int b = 0; b < bound; b++) {
//				//			for (int i = 0; i < NUM_ACCESSES / 2; i++) {
////				read_only(transaction, ((MicroParam) db.param).keys(i));
////			}
//				for (int i = NUM_ACCESSES / 2; i < NUM_ACCESSES; ++i) {
//					read_write(((MicroParam) param).keys(i));
////			read_write(String.valueOf(15));
//				}
//
//			}
//		} while (!transactionManager.CommitTransaction(txn_context, gap));
//		//end transaction processing.
////		//LOG.DEBUG(this.context.getThisTaskId() + " select finish one transaction of: " + bid + " at:\t" + DateTime.now());
//
//		//operator communication
//		for (int b = 0; b < bound; b++) {
//			collector.emit(bid, new StreamValues(in.getMsg(b)));
//		}
    }


    public void execute(TransferTuple in) throws InterruptedException, DatabaseException {
        //undefined.

    }
}
