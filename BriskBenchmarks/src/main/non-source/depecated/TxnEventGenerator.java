package applications.bolts.ct;


import applications.param.TransactionEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.SplittableRandom;

public class TxnEventGenerator extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(TxnEventGenerator.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    transient SplittableRandom rnd;

//    private String rightpad(String text, int length) {
//        return String.format("%-" + length + "." + length + "s", text);
//    }
//
//    private String GenerateValue(int key) {
//        return rightpad(String.valueOf(key), VALUE_LEN);
//    }

    public TxnEventGenerator(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    @Override
    public void initialize(int thisTaskId, ExecutionGraph graph) {
        rnd = new SplittableRandom(1234);
    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();

        if (in.isMarker()) {
            forward_checkpoint(in.getSourceTask(), bid, in.getMarker());
        } else {
            TransactionEvent event = randomTransactionEvent(bid, rnd);
            collector.force_emit(bid, event);
        }
    }

}
