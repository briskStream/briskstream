package applications.bolts.ct;


import applications.param.TransactionEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.transaction.dedicated.ordered.TxnManagerTStream;
import engine.transaction.function.Condition;
import engine.transaction.function.INC;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.SplittableRandom;
import java.util.concurrent.BrokenBarrierException;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;

public class TransTransaction extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(TransTransaction.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();
    transient SplittableRandom rnd;
    private List<TransactionEvent> holder = new LinkedList<>();

    public TransTransaction(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, int thisTaskId, ExecutionGraph graph) {
        transactionManager = new TxnManagerTStream(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());
        rnd = new SplittableRandom(1234);
    }

    /**
     * @param Event
     * @param bid
     * @throws DatabaseException
     */
    private void txn_request(TransactionEvent Event, long bid) throws DatabaseException {
        //begin transaction processing.
        txn_context = new TxnContext(this.context.getThisTaskId(), this.fid, bid);//create a new txn_context for this new transaction.


        String[] srcTable = new String[]{"accounts", "bookEntries"};
        String[] srcID = new String[]{Event.getSourceAccountId(), Event.getSourceBookEntryId()};


        transactionManager.Asy_ModifyRecord(txn_context, "accounts", Event.getSourceAccountId(), Event.getSourceAccountId(), new INC(Event.getAccountTransfer()), srcTable, srcID, new Condition(Event.getMinAccountBalance(), Event.getAccountTransfer(), Event.getBookEntryTransfer()), Event.success);          //asynchronously return.

        transactionManager.Asy_ModifyRecord(txn_context, "accounts", Event.getTargetAccountId(), Event.getTargetAccountId(), new INC(Event.getAccountTransfer()), srcTable, srcID, new Condition(Event.getMinAccountBalance(), Event.getAccountTransfer(), Event.getBookEntryTransfer()), Event.success);          //asynchronously return.

        transactionManager.Asy_ModifyRecord(txn_context, "bookEntries", Event.getSourceBookEntryId(), Event.getSourceBookEntryId(), new INC(Event.getBookEntryTransfer()), srcTable, srcID, new Condition(Event.getMinAccountBalance(), Event.getAccountTransfer(), Event.getBookEntryTransfer()), Event.success);   //asynchronously return.

        transactionManager.Asy_ModifyRecord(txn_context, "bookEntries", Event.getTargetBookEntryId(), Event.getTargetBookEntryId(), new INC(Event.getBookEntryTransfer()), srcTable, srcID, new Condition(Event.getMinAccountBalance(), Event.getAccountTransfer(), Event.getBookEntryTransfer()), Event.success);   //asynchronously return.

    }

    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException, BrokenBarrierException {
        long bid = in.getBID();
        if (in.isMarker()) {
            int i = 0;
            ////LOG.DEBUG("Starts TP evaluation.. Currently holding: " + holder.size() + " transactions.");
            transactionManager.start_evaluate(this.context.getThisTaskId(), this.fid, bid);//start lazy evaluation in transaction manager.

            //Perform computation on each event and emit.
            for (TransactionEvent Event : holder) {
                // check the preconditions
                if (Event.success[0]) {
                    collector.force_emit(Event.getTimestamp(), Event);      // emit result event with updated balances and flag to mark transaction as processed
                } else {
                    collector.force_emit(Event.getTimestamp(), Event);      // emit result with unchanged balances and a flag to mark transaction as rejected
                }

            }

            holder.clear();//all tuples in the holder is finished.
            forward_checkpoint(in.getSourceTask(), bid, in.getMarker());
        } else {
            TransactionEvent Event = randomTransactionEvent(bid, rnd);//(TransactionEvent) in.getValue(0);
            txn_request(Event, bid);
            holder.add(Event);//mark the tuple as ``in-complete".... This function requires return..
        }
    }

}
