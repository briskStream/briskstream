package applications.bolts.mb.deprecated;


import applications.param.MicroEvent;
import brisk.components.operators.api.TransactionProcessBolt;
import brisk.execution.ExecutionGraph;
import brisk.execution.runtime.tuple.impl.Tuple;
import brisk.faulttolerance.impl.ValueState;
import engine.DatabaseException;
import engine.storage.datatype.DataBox;
import engine.transaction.dedicated.ordered.TxnManagerOrderLockBlocking;
import engine.transaction.impl.TxnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

import static applications.constants.MicroBenchmarkConstants.Constant.VALUE_LEN;
import static engine.Meta.MetaTypes.AccessType.READ_WRITE;

public class WriteBolt_olb extends TransactionProcessBolt {
    private static final Logger LOG = LoggerFactory.getLogger(WriteBolt_olb.class);
    private static final long serialVersionUID = -5968750340131744744L;
    LinkedList<Long> gap = new LinkedList<>();

    public WriteBolt_olb(int fid) {
        super(LOG, fid);
        state = new ValueState();
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    private String GenerateValue(int key) {
        return rightpad(String.valueOf(key), VALUE_LEN);
    }

    @Override
    public void initialize(int thread_Id, ExecutionGraph graph) {
        super.initialize(thread_Id, graph);
        transactionManager = new TxnManagerOrderLockBlocking(db.getStorageManager(), this.context.getThisComponentId(), thread_Id, this.context.getThisComponent().getNumTasks());
    }

    private void lock_ahead(MicroEvent Event, long bid) throws DatabaseException {
        for (int i = 0; i < NUM_ACCESSES; ++i)
            transactionManager.lock_ahead(txn_context, "MicroTable", String.valueOf(Event.getKeys()[i]), Event.getRecord_refs()[i], READ_WRITE);
    }


    private void txn_request(MicroEvent Event, long bid) throws DatabaseException {

        for (int i = 0; i < NUM_ACCESSES; ++i) {
            String key = String.valueOf(Event.getKeys()[i]);
            boolean rt = transactionManager.SelectKeyRecord_noLock(txn_context, "MicroTable", key, Event.getRecord_refs()[i], READ_WRITE);
            assert rt;
            assert Event.getRecord_refs()[i].record != null;
        }
    }


    @Override
    public void execute(Tuple in) throws InterruptedException, DatabaseException {
        long bid = in.getBID();
        String componentId = context.getThisComponentId();
        MicroEvent Event = generateEvent(bid);
        //begin transaction processing.
        long start = System.nanoTime();

        txn_context = new TxnContext(componentId, this.fid, bid);

        transactionManager.getOrderLock().blocking_wait(bid);//ensures that locks are added in the event sequence order.
        lock_ahead(Event, bid);
        transactionManager.getOrderLock().advance();//ensures that locks are added in the event sequence order.
        long order_wait = 0;

        if (metrics.measure) {
            long end = System.nanoTime();
            order_wait = (end - start);
        }

        txn_request(Event, bid);

        transactionManager.CommitTransaction(txn_context);

        long start2 = System.nanoTime();
        for (int i = 0; i < NUM_ACCESSES; ++i) {
            List<DataBox> values = Event.getValues()[i];
            Event.getRecord_refs()[i].record.updateValues(values);
        }
        if (metrics.measure) {
            long end = System.nanoTime();
            double total_time = (end - start) / 1E6;

            metrics.useful_time.get(componentId).addValue((end - start2) / total_time);//store percentage directly.

//            metrics.ts_allocation.get(componentId).addValue(txn_context.ts_allocation / exe_time);//store percentage directly.

            metrics.index_time.get(componentId).addValue(txn_context.index_time / total_time);//store percentage directly.

            metrics.wait.get(componentId).addValue((txn_context.lock_time + order_wait) / total_time);//store percentage directly.
        }
        collector.force_emit(Event.getTimestamp(), Event);//the tuple is finished.
    }


}
